import { type } from "os";

let w: unknown = 1;
w = "string"; 
w =  {
    runANonExistentmethod: () => {
        console.log("I think therefore I am");
    }
} as { runANonExistentmethod: () => void}

if(typeof w === 'object' && w!== null) {
    (w as { runANonExistentmethod: () => void}).runANonExistentmethod();
}
